from serpent.game_api import GameAPI
from serpent.input_controller import KeyboardKey
from serpent.input_controller import MouseButton

# Diverse constante de care vom avea nevoie in joc
SUCCESSFUL_RELOAD = 1
NOT_ENOUGH_AMMO = 2
NOT_RELOADABLE = 3

MELEE_WEAPON = 1
MAIN_WEAPON = 2
SECONDARY_WEAPON = 3
TROWABLE_WEAPON = 4

BANDAGE = 7
MEDKIT = 8
ENERGY_DRINK = 9
PILLS = 10

class SurvivIOAPI(GameAPI):

    def __init__(self, game=None):
        super().__init__(game=game)

    def my_api_function(self):
        pass

    class MyAPINamespace:

        @classmethod
        def my_namespaced_api_function(cls):
            api = SurvivIOAPI.instance

    # Definim actiunile pe care Agentul le poate efectua
    class Actions:
        input_controller = None
        directions = {
            'NORTH': KeyboardKey.KEY_UP,
            'SOUTH': KeyboardKey.KEY_DOWN,
            'EAST': KeyboardKey.KEY_RIGHT,
            'WEST': KeyboardKey.KEY_LEFT,
        }

        # Primeste o directie si aplica miscarea.
        # Pentru a merge in diagonala, se va apela de 2 ori functia
        @classmethod
        def move(cls, direction, time=0):
            # Daca directiile sunt valide, ne deplasam
            if direction in cls.directions:
                if time == 0:
                    # Vrem sa ne deplasam continuu
                    cls.input_controller.press_key(cls.directions[direction])
                else:
                    # Vrem sa ne deplasam pentru o perioada de timp
                    cls.input_controller.tap_key(cls.directions[direction], time)

        # Va intrerupe orice miscare a Agentului
        @classmethod
        def cancel_movement(cls):
            cls.input_controller.release_keys(cls.directions.values())

        # Agentul va incerca sa reincarce arma
        @classmethod
        def reload(cls):
            cls.input_controller.tap_key(KeyboardKey.KEY_R)

        # Agentul va incerca sa adune o resursa de jos
        @classmethod
        def pickup_resource(cls):
            cls.input_controller.tap_key(KeyboardKey.KEY_F)

        # Agentul va schimba resursa actuala (armura, casca, rucsac, arma) cu cea de jos
        @classmethod
        def change_resource(cls):
            cls.input_controller.tap_key(KeyboardKey.KEY_F)

        # Agentul va incerca sa deschida o usa
        @classmethod
        def open_door(cls):
            cls.input_controller.tap_key(KeyboardKey.KEY_F)

        # Agentul va incerca sa inchida o usa
        @classmethod
        def close_door(cls):
            cls.input_controller.tap_key(KeyboardKey.KEY_F)

        # Agentul isi va echipa o arma
        @classmethod
        def equip_weapon(cls, weapon_type):
            if weapon_type == MELEE_WEAPON:
                cls.input_controller.tap_key(KeyboardKey.KEY_1)
            elif weapon_type == MAIN_WEAPON:
                cls.input_controller.tap_key(KeyboardKey.KEY_2)
            elif weapon_type == SECONDARY_WEAPON:
                cls.input_controller.tap_key(KeyboardKey.KEY_3)
            elif weapon_type == TROWABLE_WEAPON:
                cls.input_controller.tap_key(KeyboardKey.KEY_4)

        # Agentul va trage / lovi un anumit timp
        # Numarul de lovitul va fi stabilit in functie de fire_rate-ul armei
        @classmethod
        def fire_weapon(cls, time=0.05):
            cls.input_controller.click(MouseButton.LEFT, duration=time)


        # Botul va folosi una dintre resursele consumabile
        @classmethod
        def use_consumable(cls, consumable_type):
            if consumable_type == BANDAGE:
                cls.input_controller.tap_key(KeyboardKey.KEY_7)
            elif consumable_type == MEDKIT:
                cls.input_controller.tap_key(KeyboardKey.KEY_8)
            elif consumable_type == ENERGY_DRINK:
                cls.input_controller.tap_key(KeyboardKey.KEY_9)
            elif consumable_type == PILLS:
                cls.input_controller.tap_key(KeyboardKey.KEY_0)
