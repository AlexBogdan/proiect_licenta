from serpent.game import Game

from .api.api import SurvivIOAPI

from serpent.utilities import Singleton



class SerpentSurvivIOGame(Game, metaclass=Singleton):

    def __init__(self, **kwargs):
        kwargs["platform"] = "executable"
        kwargs["window_name"] = "surviv.io - 2d battle royale game"
        kwargs["executable_path"] = "/home/licentiatul/Desktop/surviv-io-linux-x64/surviv-io"

        super().__init__(**kwargs)

        self.api_class = SurvivIOAPI
        self.api_instance = None

    # TODO: Va trebui sa definim regiunile importante pentru analiza statisticilor
    # personajului in timpul jocului
    @property
    def screen_regions(self):
        regions = {
            "SAMPLE_REGION": (0, 0, 0, 0)
        }

        return regions

    @property
    def ocr_presets(self):
        presets = {
            "SAMPLE_PRESET": {
                "extract": {
                    "gradient_size": 1,
                    "closing_size": 1
                },
                "perform": {
                    "scale": 10,
                    "order": 1,
                    "horizontal_closing": 1,
                    "vertical_closing": 1
                }
            }
        }

        return presets
