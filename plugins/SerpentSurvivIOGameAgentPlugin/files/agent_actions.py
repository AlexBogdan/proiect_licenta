from serpent.input_controller import KeyboardKey

class AgentActions():
    def __init__(self, input_controller):
        self.input_controller = input_controller
        self.directions = {
            'NORTH': KeyboardKey.KEY_UP,
            'SOUTH': KeyboardKey.KEY_DOWN,
            'EAST': KeyboardKey.KEY_RIGHT,
            'WEST': KeyboardKey.KEY_LEFT,
        }

    # Primeste o lista de directii (de dim 1 sau 2) si aplica miscarea
    def move(self, directions, time=0):
        # Initial vom intrerupe orice alta miscare in derulare
        self.input_controller.release_keys(self.directions.values())

        # Extragem tastele directiilor in care vrem sa deplasam botul
        directions_codes = [self.directions[dir] for dir in directions]

        # Daca directiile sunt valide, ne deplasam
        if directions_codes != []:
            if time == 0:
                # Vrem sa ne deplasam continuu
                self.input_controller.press_keys(directions_codes)
            else:
                # Vrem sa ne deplasam pentru o perioada de timp
                self.input_controller.tap_keys(directions_codes, time)
