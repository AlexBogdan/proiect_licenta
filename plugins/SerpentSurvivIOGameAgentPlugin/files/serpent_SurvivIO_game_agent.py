from serpent.game_agent import GameAgent
from serpent.input_controller import KeyboardKey
from serpent.input_controller import MouseButton
# from agent_actions.py import AgentActions
import random
import serpent.cv

import skimage.io
import numpy as np
from skimage import data
from skimage.feature import match_template
import cv2
import time


# Diverse constante de care vom avea nevoie in joc
SUCCESSFUL_RELOAD = 1
NOT_ENOUGH_AMMO = 2
NOT_RELOADABLE = 3

MELEE_WEAPON = 1
MAIN_WEAPON = 2
SECONDARY_WEAPON = 3
TROWABLE_WEAPON = 4

BANDAGE = 7
MEDKIT = 8
ENERGY_DRINK = 9
PILLS = 10

AMMO_9MM = 1
AMMO_12GAUGE = 2
AMMO_762MM = 3
AMMO_556MM = 4
# 5 fiind de culoare neagra, este detectat gresit mereu
# AMMO_50AE = 5
AMMO_FLARE = 6
# 7 si 8 s-ar putea sa nu ne intereseze
AMMO_45ACP = 7
AMMO_308SUB = 8

AMMO_SPRITES = {
    AMMO_9MM:       "SPRITE_AMMO_9MM",
    AMMO_12GAUGE:   "SPRITE_AMMO_12GAUGE",
    AMMO_762MM:     "SPRITE_AMMO_762MM",
    AMMO_556MM:     "SPRITE_AMMO_556MM",
    # AMMO_50AE:      "SPRITE_AMMO_50AE",
    AMMO_FLARE:     "SPRITE_AMMO_FLARE",
    AMMO_45ACP:     "SPRITE_AMMO_45ACP",
    AMMO_308SUB:    "SPRITE_AMMO_308SUB"
}

class SerpentSurvivIOGameAgent(GameAgent):

    # Pe asta va trebui sa o rescriem, sa fie mai apropiata de ce avem nevoie
    # Poate o vom si muta de aici undeva prin API
    def locate(self, sprite=None, game_frame=None, threshold = 0.90, threshold_alpha = 0.999, grayscale = True):
        """
        kinda like locate function below only better
        threshold is the match threshold for normal searches
        threshold_alpha is the match threshold for sprites with alpha channels
        they're different numbers because the search algorithms produce results in different regions
        """
        location = None
        use_alpha_mask = False
        sprite_data_shape = sprite.image_data.shape
        if sprite_data_shape[2] == 4: # the sprite has an alpha channel
            template_alpha = sprite.image_data[:,:,3,0]
            if np.count_nonzero(template_alpha) != sprite_data_shape[0]*sprite_data_shape[1]:
                # only use alpha masked search method if
                # there is an alpha channel in the sprite and it's in use
                # because the search method that supports masking is slower
                use_alpha_mask = True
                threshold = threshold_alpha
        template = sprite.image_data[:,:,:3,0]
        frame = game_frame.frame

        # grayscale matching is faster and seems to be good enough all the time
        if grayscale == True:
            template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        elif use_alpha_mask == True:
            # need to make sure alpha channel dim matches in non-grayscale case
            template_alpha = np.stack((template_alpha, template_alpha, template_alpha),axis=2)

        if use_alpha_mask == False:
            search_result = cv2.matchTemplate(frame, template, cv2.TM_CCOEFF_NORMED)
        else:
            search_result = cv2.matchTemplate(frame, template, cv2.TM_CCORR_NORMED, mask=template_alpha)

        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(search_result)

        print(max_val)
        if max_val >= threshold:
            location = (max_loc[1], max_loc[0], max_loc[1]+sprite_data_shape[0], max_loc[0]+sprite_data_shape[1])

        return location


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.frame_handlers["PLAY"] = self.handle_play
        self.frame_handler_setups["PLAY"] = self.setup_play

        # Redimensionam fereastra jocului
        self.game.window_controller.resize_window(self.game.window_id, 640, 480)


    def setup_play(self):
        self.game.api_class.Actions.input_controller = self.input_controller
        # Agentul se va afla mereu in mijlocul ecranului
        self.player_coords = (self.game.window_geometry['width'] / 2, self.game.window_geometry['height'] / 2)
        # Directiile in care poate sa se deplaseze Agentul
        self.directions = ['NORTH', 'SOUTH', 'WEST', 'EAST']
        # Viteza (px / s) pe care player-ul o are pentru zoom x1 (fara energy buff)
        self.speed = 246


    # Functiile astea care sunt combinatii de actiuni ar trebui catalogate cumva si mutate in API
    # Ele reprezinta si strategia in final (modul in care sunt legate)
    # Vom defini o gramatica in care ele vor fi Neterminali si actiunile atomice (din Actions) Terminali
    # Agentul va genera cod din ele, facand un Behavioural Tree

    # Transformam obtinem centrul unui zone, returnata ca (x, y)
    def get_location_center(self, location):
        return ((location[1] + location[3]) / 2, (location[0] + location[2]) / 2)

    # Obtinem distanta dintre 2 puncte definite ca tuplu (x, y)
    def get_distance(self, point1, point2):
        return np.linalg.norm(np.array(point1) - np.array(point2))

    # Vom intoarce directiile in care se afla punctul fata de Agent si distanta pe cele 2 axe
    def get_directions(self, point):
        directions = {}

        if self.player_coords[0] > point[0]:
            directions['WEST'] = abs(self.player_coords[0] - point[0])
        elif self.player_coords[0] < point[0]:
            directions['EAST'] = abs(self.player_coords[0] - point[0])

        if self.player_coords[1] > point[1]:
            directions['NORTH'] = abs(self.player_coords[1] - point[1])
        elif self.player_coords[1] < point[1]:
            directions['SOUTH'] = abs(self.player_coords[1] - point[1])

        return directions

    # Agentul va merge spre punctul dorit
    # Vom intoarce True daca ne deplasam sau False daca suntem deja in zona
    def goto_location(self, location):
        # Obtinem centrul zonei in care mergem
        location_center = self.get_location_center(location)
        # Obtinem distanta pana in acel punct
        distance = self.get_distance(self.player_coords, location_center)
        # Daca nu snutem deja suficient de aproape, ne deplasam
        if distance > 57:
            directions = self.get_directions(location_center)
            for direction, distance in directions.items():
                self.game.api_class.Actions.move(direction, (distance - 5) / self.speed)

            time.sleep(distance / self.speed)
            return True
        else:
            return False


    def destroy_box(self, game_frame):
        location = self.locate(
            sprite = self.game.sprites['SPRITE_NORMAL_CRATE'],
            game_frame = game_frame,
            threshold = 0.70,
            threshold_alpha = 0.999,
            grayscale = True
        )
        if location is not None:
            if self.goto_location(location) == False:
                self.game.api_class.Actions.fire_weapon()

        print (list(self.game.sprites['SPRITE_NORMAL_CRATE'].image_shape))


    # Cautam random gloante pe harta, de orice tip, fara a le prioritiza importanta
    # Momentan doar vom aduna de pe jos
    def search_ammo(self, game_frame):
        for (ammo_type, ammo_sprite) in AMMO_SPRITES.items():
            print(ammo_type)
            ammo_location = self.locate(
                sprite = self.game.sprites[ammo_sprite],
                game_frame = game_frame,
                threshold = 0.90,
                threshold_alpha = 0.94,
                grayscale = False
            )
            if ammo_location is not None:
                center = self.get_location_center(ammo_location)
                self.input_controller.move(center[0], center[1])
                self.goto_location(ammo_location)
                time.sleep(0.1)
                self.game.api_class.Actions.pickup_resource()
                break


    def explore(self, game_frame):
        self.search_ammo(game_frame)
        # self.game.api_class.Actions.move(random.choice(self.directions), random.randint(1, 3))

    # Functia care defineste comportamentul agentului
    def handle_play(self, game_frame):
        print("\n\n\nSalut sefule!")
        self.explore(game_frame)
