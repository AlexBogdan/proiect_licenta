---
title: "Introducere SurvivIOBot"
date: "2019-06-02 01:30"
---
# SurvivIO Bot - IA / GameTheory / Computer Vision / Program Synthesis (sau ML)

Readme-ul de aici va contine in final detalii de implementare. Cand o sectiune de aici
este finalizata, aceasta merge in GoogleDoc, care la final va fi retusat in MS Word.

## Problem Statement
  Jocurile multiplayer pot fi o mare problema pentru NPC-urile / botii ce trebuie
sa aiba o strategie cat mai buna in momentul in care vor juca impotriva unor
oameni reali. In general majoritatea jocurilor dispun de boti creati de catre
programatori inainte de a lansa jocul, acesta fiind predefiniti si avand o
strategie statica. Chiar daca acestia vin pe diverse categorii
(easy / medium / hard / insane / etc.) lipsa lor de adaptare poate duce in final
la limitarea lor in momentul interactiunii cu jucatori foarte experimentati.

  O solutie pentru a rezolva aceasta problema ar fi crearea unui bot care sa isi
genereze propria strategie imbunatatita cu timpul. Folosindu-se mereu de
strategiile observate la jucatorii mai buni decat el, asimiland cele mai bune
actiuni pe care oamenii le iau si generand astfel un nou cod pentru strategia
imbunatatita, botul va putea tine pasul cu cei mai buni jucatori existenti.

## OpenAI Five

  Compania OpenAI reuseste sa dezvolte botul [OpenAI Five][1505aca3] pentru jocul
Dota 2 (unul dintre cele mai populare jocuri din istorie), care devine AI-ul care
reuseste sa invinga aproape orice om in acest joc foarte complex, depasind orice
nivel de bot predefinit in joc.

  OpenAI Five, conform explicatiilor echipei [explicatiilor echipei][ab112c64],
este o echipa formata din 5 copii ale unui bot reprezentat de o retea neurala
antrenata prin generarea a 180 de ani de gameplay pe zi. Fiecare runda jucata va
fi descrisa de starile jocului, codificate ca liste de 20.000 de numere, asupra
carora Agentul isi va aplica actiunile, codificate prin liste de 8 numere.
Fiecare stare va fi evaluata, obtinand o recompensa pozitiva sau negativa. In
functie de recompense, reteaua neurala va fi actualizata, imbunatatind astfel
strategia.

## SurvivIO Bot (nume actual)
  [Surviv.io][4dd325bf] este un joc 2D popular din categoria de jocuri Battle Royale.
Aceasta categorie ce a stabilit deja un trend este un joc, poate reprezenta un mediu
bun pentru dezvoltarea unui bot, avand un numar impresionant de jucatori
(minim 10.000) la orice ora.
  Pentru ca generarea unor simulari ca cele descrise anterior necesita resurse
hardware impresionante, botul creat de mine va avea o abordare diferita.

  - [ ] Problema eficienta browser si transformarea jocului intr-o aplicatie linux nativa folosind [nativefier][aa8b58e0]

## Abordare

  Alegand un joc 2D din categoria acestor jocuri complexe, am dorit sa creez un
bot care sa tinda spre performantele OpenAI Five, dar printr-o alta abordare.

Intrucat jucatorii experimentati au deja strategii foarte bune in acest joc,
urmarirea rundelor jucate de acestia si extragerea informatiilor din aceste
runde pot furniza date extrem de valoroase. Simularea aleatoare a milioanelor de
runde va fi astfel inlocuita cu extragerea datelor din rundele jucate de oameni
reali.
  Componentele necesare unei astfel de abordari vor fi descrise in continuare.

## SurvivIO

  - [ ] Descrierea categoriei de jocuri Battle Royale, pe scurt
  - [ ] Descrierea jocului si de ce este o varianta mai simpla, pe scurt
  - [ ] Descrierea actiunilor si partilor care vor prezenta interes in aceasta lucrare, in detaliu


## SerpentAI
  [SerpentAI][1606ce05] este un Framework dezvoltat de catre un numar redus de
programatori pasionati de domeniul AI din jocuri video. Dupa o scurta viata de
1 an si 7 luni, acest framework ofera o modalitate foarte simpla de a crea un
bot, facand legatura dintre codul acestuita si jocul pentru care a fost conceput.


  - [ ] Sa descriem cum este gandit acest Framework (sistemul de plug-inuri)
  - [ ] Sa vorbim despre GamePlugin si ce am implementat aici. Asta va creste cu timpul
  - [ ] Sa vorbim despre GameAgentPlugin si ce am implementat aici. Asta va creste cu timpul
  - [ ] Sa vorbim despre partea de InputController, despre cum botul interactioneaza cu jocul
  - [ ] Sa vorbim despre Sprite-uri si incarcarea lor automata
  - [ ] Sa vorbim despre partea de ML, in caz ca voi folosi componenta aceasta
  - [ ] Sa vorbim despre partea de OCR

## Computer Vision

  ### OpenCV

  - [ ] Descrierea bibliotecii
  - [ ] De ce am folosit OpenCV in loc de skimage care este deja folosit in SerpentAI
  - [ ] Cum functioneaza detectia Sprite-urilor in game_frame-uri

  ### Reprezentarea si recunoasterea actiunilor (TODO)

  ### Reprezentarea si recunoasterea starilor (TODO)


## Generare Strategie

  - [ ] Gramatica actiunilor
  - [ ] Graful (Automatul) pentru stari / actiuni + update-ul acestora
  - [ ] DACA ESTE SIMPLU, O RETEA NEURALA AR MERGE AICI
  - [ ] Behavioral Trees? E o solutie?
  - [ ] Alte exemple de jocuri in care se folosesc toate astea


## Program Synthesis

  - [ ] Asociere template-uri de cod pentru noduri decizionale si actiuni
  - [ ] Trecerea strategiei in cod. Asta daca nu facem cumva o retea neurala
  - [ ] Transpunerea strategiei in cod efectiv prin stabilirea succesiunii starilor si actiunile luate, modificand valori de parametri

## Implementare

  - [ ] Cum am reusit sa fac toate astea
  - [ ] De ce probleme m-am lovit (punem accent pe astea)
  - [ ] Exemple de cod relevante (daca e necesar)
  - [ ] Cum arata codul generat de bot, daca ajung sa fac asta

## Rezultate

### Evaluarea jocului meu
  Date obtinute prin analizarea rundelor jucate de mine
### Evaluarea jocului random
  Botul va lua decizii pseudo-random, avand o strategie rudimentara implementata de mana de catre mine
### Evaluarea jocului unui incepator
Un prieten va incerca sa joace cateva runde (minim 100) pentru a observa o eventuala evolutie in strategia lui
### Evaluarea jocului botului
Botul va incerca sa priveasca cat mai multe jocuri, jucand si el de 5 ori dupa fiecare 5 runde de invatare, pentru a vedea evolutia
### Comparatia celor 4
### Comparatia incepator - bot
  Aici poate deveni interesant, amandoi pronind de la 0, botul dispune de
precizia cu care ia decizii, incepatorul dispune de ratiune.

## Concluzii

## Bibliografie


[1505aca3]: https://openai.com/five/ "OpenAI Five"
[ab112c64]: https://openai.com/five/#how-openai-five-works "explicatii"
[4dd325bf]: http://surviv.io/ "SurvivIO"
[1606ce05]: https://github.com/SerpentAI/SerpentAI "SerpentAI"
[aa8b58e0]: https://github.com/jiahaog/nativefier "nativefier"
